// import and configure firebase
import * as firebase from 'firebase';

const firebaseConfig = {
  apiKey: "AIzaSyCcjm5tYeq2snrrpVyyZFVyVeY-ElMaVp0",
  authDomain: "chatapp-f4ef4.firebaseapp.com",
  databaseURL: "https://chatapp-f4ef4.firebaseio.com",
  projectId: "chatapp-f4ef4",
  storageBucket: "chatapp-f4ef4.appspot.com",
  messagingSenderId: "367385808770"
};

export const firebaseApp = firebase.initializeApp(firebaseConfig);
