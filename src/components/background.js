import React, { Component } from 'react'
import {
  ImageBackground,
  View,
  StyleSheet
} from 'react-native'

export default class Background extends Component {
  constructor(props) {
    super(props)
  }

  render() {
    return (
      <ImageBackground
        source={this.props.imgSrouce}
        style={styles.backgroundImg}>
        <View style={styles.backgroundContainer}>
          {this.props.children}
        </View>
      </ImageBackground>
    )
  }
}

const styles = StyleSheet.create({
  backgroundContainer : {
    position: 'absolute',
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
    backgroundColor: 'rgba(0, 0, 0, 0.3)'
  },
  backgroundImg : {
    flex: 1,
    height: null,
    width: null
  }
});
