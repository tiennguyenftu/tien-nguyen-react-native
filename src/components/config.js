/**
 * configuration file
 * colors, themes, base styles etc
 */

// colors
const colors = {
  'googleBlue100': {
    'color': '#C5CAE9'
  },
  'googleBlue300': {
    'color': '#7986CB'
  },
  'googleBlue500': {
    'color': '#3F51B5'
  },
  'googleBlue700': {
    'color': '#303F9F'
  }
};

const PRIMARY = 'googleBlue';

// helper function to get colors
export function getColor(string) {
  if (string) {
    if (string.indexOf('#') > -1 || string.indexOf('rgba') > -1
        || string.indexOf('rgb') > -1) {
      return string
    }
    if (colors[string]) {
      return colors[string].color
    }
    if (colors[`${string}500`]) {
      return colors[`${string}500`].color
    }
  }
  return colors[`${PRIMARY}500`].color
}

//Fonts
export const Fonts = {
  general: {
    light: 'raleway-light',
    regular: 'raleway',
    medium: 'raleway-medium'
  },
  logo: 'billabong'
};
