# Prerequisites
NodeJS: >= v6.x  
Global packages: `react-native-cli`, `yarn`
```bash
npm i -g react-native-cli
npm i -g yarn
```

# How to Run
``` 
yarn install
react-native run-ios 
# or
react-native run-android
```

# Knowledge used
- Using custom fonts in React Native
- Authentication & data storage with `firebase`
- State Management with `redux`
- Redux Storage (with async-storage engine for react native for application persistence)
- Animation with `LayoutAnimation` in React Native & `react-native-animatable` package
- Navigation with `Navigator`